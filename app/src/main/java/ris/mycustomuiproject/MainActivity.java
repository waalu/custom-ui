package ris.mycustomuiproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private MySimpleCustomView mCustomView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mCustomView = (MySimpleCustomView) findViewById(R.id.custom_view);
    }

    public void onClick(View view){
        mCustomView.toggle();
    }
}
