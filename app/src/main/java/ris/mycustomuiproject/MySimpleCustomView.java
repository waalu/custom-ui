package ris.mycustomuiproject;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.Checkable;

/**
 * Created by rodin_000 on 8/21/2016.
 */

public class MySimpleCustomView extends View implements Checkable{
    
    private static final String TAG = MySimpleCustomView.class.getSimpleName();


    private Drawable mBackground;
    private boolean isChecked;

    private final int[] MY_ATTR_SET = new int[]{
        android.R.attr.background
    };
    
    public MySimpleCustomView(Context context) {
        super(context);
        Log.d(TAG,"MySimpleCustomView");

    }

    public MySimpleCustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Log.d(TAG,"MySimpleCustomView(2)"+getTag());
        TypedArray a = context.obtainStyledAttributes(attrs,MY_ATTR_SET);
        mBackground = a.getDrawable(0);
        a.recycle();

    }

    public MySimpleCustomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Log.d(TAG,"MySimpleCustomView(3)"+getTag());

    }

    @Override
    protected void onDraw(Canvas canvas) {
        Log.d(TAG,"onDraw"+getTag());
        mBackground.draw(canvas);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Log.d(TAG,"onMeasure: "+getTag()+":"+widthMeasureSpec+" : "+heightMeasureSpec);
        int sizeW = MeasureSpec.getSize(widthMeasureSpec);
        int sizeH = MeasureSpec.getSize(heightMeasureSpec);
        int modeW = MeasureSpec.getMode(widthMeasureSpec);
        int modeH = MeasureSpec.getMode(heightMeasureSpec);
        Log.d(TAG,"before: "+getTag()+":"+sizeW+" : "+sizeH+" : "+modeW+" : "+modeH);
        int resolcedW = 0;
        int resolcedH = 0;
        switch(modeW){
            case MeasureSpec.AT_MOST:{
                resolcedW = sizeW;
                break;
            }
            case MeasureSpec.EXACTLY:{
                resolcedW = Math.min(sizeW,getMinimumWidth());
                break;
            }
            case MeasureSpec.UNSPECIFIED:{
                resolcedW = getMinimumWidth();
                break;
            }
        }
        switch(modeH){
            case MeasureSpec.AT_MOST:{
                resolcedH = sizeH;
                break;
            }
            case MeasureSpec.EXACTLY:{
                resolcedH = Math.min(sizeH,getMinimumHeight());
                break;
            }
            case MeasureSpec.UNSPECIFIED:{
                resolcedH = getMinimumHeight();

                break;
            }
        }
        Log.d(TAG,"after: "+getTag()+":"+resolcedW+" : "+resolcedH);
        setMeasuredDimension(resolcedW,resolcedH);
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        Log.d(TAG,"onSizeChanged: "+getTag()+":"+w+" : "+h);
        mBackground.setBounds(0,0,w,h);
    }

//    @Override
//    protected void drawableStateChanged() {
//        int currentDrawaleState[] = getDrawableState();
//        int[] state = new int[currentDrawaleState.length+1];
//        if(isChecked()){
//            state = mergeDrawableStates(currentDrawaleState,new int[]{
//                    android.R.attr.state_checked
//            });
//        } else {
//            state = mergeDrawableStates(currentDrawaleState,new int[]{
//                    -android.R.attr.state_checked
//            });
//        }
//    }

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
       int[] state = super.onCreateDrawableState(extraSpace+1);
        if(isChecked()){
            mergeDrawableStates(state,new int[]{
                    android.R.attr.state_checked
            });
        } else {
            mergeDrawableStates(state,new int[]{
                    -android.R.attr.state_checked
            });
        }
        return state;
    }

    @Override
    public void setChecked(boolean b) {
        isChecked = b;
        refreshDrawableState();
        mBackground.setState(getDrawableState());

    }

    @Override
    public boolean isChecked() {
        return isChecked;
    }

    @Override
    public void toggle() {
        setChecked(!isChecked);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.d(TAG,"onTouchEvent: "+getTag()+":"+event);
        return true;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        Log.d(TAG,"dispatchTouchEvent: "+getTag()+":"+event);
        return super.dispatchTouchEvent(event);
    }
}
