package ris.mycustomuiproject;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Parcelable;
import android.support.annotation.ColorInt;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by rodin_000 on 8/20/2016.
 */

public class MyCustomComposeView extends LinearLayout {

    private static final String TAG = MyCustomComposeView.class.getSimpleName();

    private TextView mLeftTextView;
    private TextView mRightTextView;
    private Button mButton;


    public MyCustomComposeView(Context context) {
        this(context,null);
        Log.d(TAG,"MyCustomComposeView");
    }

    public MyCustomComposeView(Context context, AttributeSet attrs) {
        this(context, attrs,-1);
        Log.d(TAG,"MyCustomComposeView(2)");

    }

    public MyCustomComposeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Log.d(TAG,"MyCustomComposeView(3)");
        init( context,  attrs,  defStyleAttr);
    }



    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        inflate(context,R.layout.custom_compose_view,this);
        mLeftTextView = (TextView) findViewById(R.id.left_tv);
        mRightTextView= (TextView) findViewById(R.id.right_tv);
        mButton = (Button) findViewById(R.id.button_id);
        TypedArray a = context.obtainStyledAttributes(attrs,R.styleable.MyCustomComposeView);
        int color = a.getColor(R.styleable.MyCustomComposeView_android_textColor, Color.BLACK);
        String textSimple = a.getString(R.styleable.MyCustomComposeView_textLeftButton);
        CharSequence textFormatted = a.getText(R.styleable.MyCustomComposeView_textLeftButton);
        if(isInEditMode()){
            color = Color.RED;
            textSimple = "Test left";
            textFormatted= "Test right";
        }
        setLeftText(textSimple);
        setRightText(textFormatted);
        setButtonColor(color);
        a.recycle();
    }

    public void setRightText(CharSequence text) {
        this.mRightTextView.setText(text);
    }

    public void setLeftText(String text) {
        this.mLeftTextView.setText(text);
    }

    public void setButtonColor(@ColorInt  int color) {
        this.mButton.setTextColor(color);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        Log.d(TAG,"onAttachedToWindow");
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        return super.onSaveInstanceState();
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        super.onRestoreInstanceState(state);
    }
}
