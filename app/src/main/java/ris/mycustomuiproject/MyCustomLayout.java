package ris.mycustomuiproject;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by rodin_000 on 8/20/2016.
 */

public class MyCustomLayout extends ViewGroup {


    private static final String TAG = MyCustomLayout.class.getSimpleName();

    public MyCustomLayout(Context context) {
        super(context);
    }

    public MyCustomLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyCustomLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }



    @Override
    protected void onLayout(boolean b, int left, int top, int right, int bottom) {
        Log.d(TAG,"onLayout: "+b+" : "+left+" : "+top+ " : "+right+" : "+bottom);
        int count = getChildCount();
        float globalXShift = 0;
        float globalYShift = 0;
        for(int i=0;i<count;i++){
            final View child = getChildAt(i);
            if(child.getVisibility()!=GONE) {
                final LayoutParams childLayoutParams = (LayoutParams) child.getLayoutParams();
                globalXShift += childLayoutParams.getShiftX();
                globalYShift += childLayoutParams.getShiftY();
                child.layout((int)globalXShift,(int)globalYShift,(int)globalXShift+child.getMeasuredWidth(),
                        (int)  globalYShift+child.getMeasuredWidth() );
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Log.d(TAG,"onMeasure: "+widthMeasureSpec+" : "+heightMeasureSpec);
        int count = getChildCount();
        for(int i=0;i<count;i++){
            final View child = getChildAt(i);
            if(child.getVisibility()!=GONE) {
                Log.d(TAG,"measureChildWithMargins");
                measureChildWithMargins(child, widthMeasureSpec, 0, heightMeasureSpec, 0);
                final LayoutParams childLayoutParams = (LayoutParams) child.getLayoutParams();
                Log.d(TAG,"after: "+child.getMeasuredWidth()+" : "+child.getMeasuredHeight());
                if (childLayoutParams.getShiftX() % 2.0f == 0) {
                    childLayoutParams.leftMargin = 20;
                } else {
                    childLayoutParams.leftMargin = 0;
                }
            }
        }
        setMeasuredDimension(widthMeasureSpec,heightMeasureSpec);
    }

    @Override
    public LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new LayoutParams(getContext(),attrs);
    }

    @Override
    protected LayoutParams generateLayoutParams(ViewGroup.LayoutParams p) {
        return new LayoutParams(p);
    }

    @Override
    protected LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
    }

    @Override
    protected boolean checkLayoutParams(ViewGroup.LayoutParams p) {
        return  p instanceof LayoutParams;
    }

    public static class LayoutParams extends MarginLayoutParams{

        private float mShiftX;
        private float mShiftY;



        public LayoutParams(Context c, AttributeSet attrs) {
            super(c, attrs);
            TypedArray a = c.obtainStyledAttributes(attrs,R.styleable.MyCustomLayoutw);
            mShiftX = a.getDimension(R.styleable.MyCustomLayoutw_layout_shiftX,0);
            mShiftY = a.getDimension(R.styleable.MyCustomLayoutw_layout_shiftY,0);
            a.recycle();

        }

        public float getShiftX(){
            return mShiftX;
        }
        public float getShiftY(){
            return mShiftY;
        }

        public LayoutParams(int width, int height) {
            super(width, height);
        }

        public LayoutParams(ViewGroup.LayoutParams source) {
            super(source);
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.d(TAG,"onTouchEvent: :"+event);
        return super.onTouchEvent(event);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        Log.d(TAG,"dispatchTouchEvent: :"+event);
        return super.dispatchTouchEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        Log.d(TAG,"onInterceptTouchEvent: :"+ev);
        return super.onInterceptTouchEvent(ev);
    }
}
